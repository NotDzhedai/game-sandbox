package main

import (
	"github.com/hajimehoshi/ebiten/v2"
	"github.com/hajimehoshi/ebiten/v2/ebitenutil"
)

const (
	PlayerIdleSideState    = 0
	PlayerWalkingSideState = 32
	PlayerIdleTopState     = 10 * 32
	PlayerWalkingTopState  = 11 * 32
	PlayerIdleDownState    = 5 * 32
	PlayerWalkingDownState = 6 * 32
)

type PlayerDirection int

const (
	PlayerRightDirection PlayerDirection = iota
	PlayerLeftDirection
	PlayerTopDirection
	PlayerDownDirection
)

type Player struct {
	sprite     *ebiten.Image
	x, y       float64
	frameCount int
	frameOY    int
	direction  PlayerDirection
	moveSpeed  float64
}

func NewPlayer() *Player {
	p := &Player{}

	playerImageSprites, _, err := ebitenutil.NewImageFromFile("archer.png")
	if err != nil {
		panic(err)
	}

	p.frameCount = 4
	p.frameOY = 0
	p.direction = PlayerRightDirection
	p.sprite = playerImageSprites
	p.moveSpeed = 0.05

	p.x = 10
	p.y = 10

	return p
}

func (p *Player) setState(px, py float64) {
	switch p.direction {
	case PlayerRightDirection:
		if px == p.x && py == p.y {
			p.frameOY = PlayerIdleSideState
		} else {
			p.frameOY = PlayerWalkingSideState
		}
	case PlayerLeftDirection:
		if px == p.x && py == p.y {
			p.frameOY = PlayerIdleSideState
		} else {
			p.frameOY = PlayerWalkingSideState
		}
	case PlayerTopDirection:
		if px == p.x && py == p.y {
			p.frameOY = PlayerIdleTopState
		} else {
			p.frameOY = PlayerWalkingTopState
		}
	case PlayerDownDirection:
		if px == p.x && py == p.y {
			p.frameOY = PlayerIdleDownState
		} else {
			p.frameOY = PlayerWalkingDownState
		}
	}
}
