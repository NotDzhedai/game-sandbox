package main

import (
	"math"

	"github.com/hajimehoshi/ebiten/v2"
	"github.com/hajimehoshi/ebiten/v2/ebitenutil"
)

type Level struct {
	w, h  int
	tiles [][]*Tile
	wall  *ebiten.Image
	floor *ebiten.Image
}

func NewLevel(w, h int) *Level {
	l := &Level{w: w, h: h}

	var err error
	l.wall, _, err = ebitenutil.NewImageFromFile("brick.png")
	if err != nil {
		panic(err)
	}

	l.floor, _, err = ebitenutil.NewImageFromFile("floor.png")
	if err != nil {
		panic(err)
	}

	l.Create()
	return l
}

func (l *Level) Create() {
	l.tiles = make([][]*Tile, l.h)

	for y := 0; y < l.h; y++ {
		l.tiles[y] = make([]*Tile, l.w)
		for x := 0; x < l.w; x++ {
			t := &Tile{}
			t.AddSprite(l.floor, false)

			switch {
			case x == 0 && y == 0:
				t.AddSprite(l.wall, true)
			case x == l.w-1 && y == 0:
				t.AddSprite(l.wall, true)
			case x == 0 && y == l.h-1:
				t.AddSprite(l.wall, true)
			case x == l.w-1 && y == l.h-1:
				t.AddSprite(l.wall, true)
			case y == 0:
				if x%(l.w/7) == 1 {
					t.AddSprite(l.wall, true)
				} else {
					t.AddSprite(l.wall, true)
				}
			case y == l.h-1:
				t.AddSprite(l.wall, true)
			case x == 0:
				t.AddSprite(l.wall, true)
			case x == l.w-1:
				t.AddSprite(l.wall, true)
			case x == 5 && y == 6:
				t.AddSprite(l.wall, true)
			default:
				t.AddSprite(l.floor, false)
			}

			l.tiles[y][x] = t
		}
	}
}

func (l *Level) Tile(x, y int) *Tile {
	if x >= 0 && y >= 0 && x < l.w && y < l.h {
		return l.tiles[y][x]
	}
	return nil
}

func (l *Level) isFloor(x float64, y float64) bool {
	t := l.Tile(int(math.Floor(x+.5)), int(math.Floor(y+.5)))

	if t == nil {
		return false
	}
	return !t.wall
}
