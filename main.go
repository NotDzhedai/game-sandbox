package main

import (
	"github.com/hajimehoshi/ebiten/v2"
)

func main() {
	g := NewGame(1200, 800)

	ebiten.SetWindowSize(g.w, g.h)
	ebiten.SetWindowResizingMode(ebiten.WindowResizingModeEnabled)

	if err := ebiten.RunGame(g); err != nil {
		panic(err)
	}
}
