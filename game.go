package main

import (
	"fmt"
	"image"
	"image/color"
	"os"

	"github.com/hajimehoshi/ebiten/v2"
	"github.com/hajimehoshi/ebiten/v2/ebitenutil"
	"github.com/hajimehoshi/ebiten/v2/inpututil"
)

type Game struct {
	w, h   int
	player *Player

	op *ebiten.DrawImageOptions

	overlayImg *ebiten.Image

	level *Level

	count int

	camScale   float64
	camScaleTo float64
}

func NewGame(w, h int) *Game {
	g := &Game{
		op:         &ebiten.DrawImageOptions{},
		overlayImg: ebiten.NewImage(100, 100),
		count:      0,
		camScale:   2,
		camScaleTo: 3,
		player:     NewPlayer(),
	}
	g.level = NewLevel(50, 50)
	g.w = w
	g.h = h
	return g
}

func (g *Game) tilePosition(x, y float64) (float64, float64) {
	return x * TileSize, y * TileSize
}

func (g *Game) Update() error {

	if inpututil.IsKeyJustPressed(ebiten.KeyEscape) {
		os.Exit(0)
	}

	var scrollY float64
	if ebiten.IsKeyPressed(ebiten.KeyC) || ebiten.IsKeyPressed(ebiten.KeyPageDown) {
		scrollY = -0.25
	} else if ebiten.IsKeyPressed(ebiten.KeyE) || ebiten.IsKeyPressed(ebiten.KeyPageUp) {
		scrollY = .25
	} else {
		_, scrollY = ebiten.Wheel()
		if scrollY < -1 {
			scrollY = -1
		} else if scrollY > 1 {
			scrollY = 1
		}
	}
	g.camScaleTo += scrollY * (g.camScaleTo / 7)

	if g.camScaleTo < 1 {
		g.camScaleTo = 1
	} else if g.camScaleTo > 4 {
		g.camScaleTo = 4
	}

	div := 10.0
	if g.camScaleTo > g.camScale {
		g.camScale += (g.camScaleTo - g.camScale) / div
	} else if g.camScaleTo < g.camScale {
		g.camScale -= (g.camScale - g.camScaleTo) / div
	}

	g.MovePlayer()

	g.count++

	return nil
}

func (g *Game) Draw(screen *ebiten.Image) {
	screen.Fill(color.Black)

	g.DrawLevel(screen)

	g.DrawPlayer(screen)

	g.overlayImg.Clear()
	ebitenutil.DebugPrint(
		g.overlayImg,
		fmt.Sprintf("FPS  %0.0f\nTPS  %0.0f\nPOS  %0.0f,%0.0f",
			ebiten.ActualFPS(),
			ebiten.ActualTPS(),
			g.player.x,
			g.player.y,
		),
	)
	g.op.GeoM.Reset()
	g.op.GeoM.Translate(10, 10)
	screen.DrawImage(g.overlayImg, g.op)
}

func (g *Game) DrawLevel(screen *ebiten.Image) {
	// level draw
	g.op.GeoM.Reset()
	for y := 0; y < g.level.h; y++ {
		for x := 0; x < g.level.w; x++ {
			t := g.level.tiles[y][x]
			g.op.GeoM.Reset()

			g.op.GeoM.Translate(g.tilePosition(float64(x), float64(y)))

			px, py := g.tilePosition(g.player.x, g.player.y)

			g.op.GeoM.Translate(-px, -py)
			g.op.GeoM.Scale(g.camScale, g.camScale)
			g.op.GeoM.Translate(float64(g.w/2.0), float64(g.h/2.0))

			screen.DrawImage(t.sprite, g.op)
		}
	}
}

func (g *Game) DrawPlayer(screen *ebiten.Image) {
	// player draw
	i := (g.count / 15) % g.player.frameCount
	sx, sy := i*TileSize, g.player.frameOY

	g.op.GeoM.Reset()
	px, py := g.tilePosition(g.player.x, g.player.y)
	if g.player.direction == PlayerLeftDirection {
		g.op.GeoM.Translate(-TileSize, 0)
		g.op.GeoM.Scale(-1, 1)
	}
	g.op.GeoM.Translate(px, py)

	g.op.GeoM.Translate(-px, -py)
	g.op.GeoM.Scale(g.camScale, g.camScale)
	g.op.GeoM.Translate(float64(g.w/2.0), float64(g.h/2.0))

	screen.DrawImage(
		g.player.sprite.SubImage(image.Rect(sx, sy, sx+TileSize, sy+TileSize)).(*ebiten.Image),
		g.op,
	)
}

func (g *Game) MovePlayer() {
	p := g.player

	px, py := g.player.x, g.player.y
	if ebiten.IsKeyPressed(ebiten.KeyArrowLeft) {
		p.direction = PlayerLeftDirection
		px -= p.moveSpeed
	}
	if ebiten.IsKeyPressed(ebiten.KeyArrowRight) {
		p.direction = PlayerRightDirection
		px += p.moveSpeed
	}
	if ebiten.IsKeyPressed(ebiten.KeyArrowUp) {
		p.direction = PlayerTopDirection
		py -= p.moveSpeed
	}
	if ebiten.IsKeyPressed(ebiten.KeyArrowDown) {
		p.direction = PlayerDownDirection
		py += p.moveSpeed
	}

	p.setState(px, py)

	if g.level.isFloor(px, py) {
		g.player.x, g.player.y = px, py
	} else if g.level.isFloor(px, g.player.y) {
		g.player.x = px
	} else if g.level.isFloor(g.player.x, py) {
		g.player.y = py
	}

}

func (g *Game) Layout(w, h int) (int, int) {
	return g.w, g.h
}
