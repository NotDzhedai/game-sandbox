package main

import "github.com/hajimehoshi/ebiten/v2"

const TileSize = 32

type Tile struct {
	sprite *ebiten.Image
	wall   bool
}

func (t *Tile) AddSprite(i *ebiten.Image, isWall bool) {
	t.sprite = i
	t.wall = isWall
}
